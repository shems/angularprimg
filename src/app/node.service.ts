import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NodeService {

  constructor(private http: HttpClient) {}
    getImage(imgName){
       this.http.get("http://localhost:3000/users/givemeimg/"+imgName, { responseType: 'blob' }).subscribe(
        (res)=>console.log(res),
        (err)=>console.error(err.error)
        
      )
    
    }
    setImage(img: any){
      var formData = new FormData();
      formData.append('file',img);
      this.http.post<any>('http://localhost:3000/users/img',formData).subscribe(
        (res)=>console.log(res),
        (err)=>console.log(err.error)
      )
    }
  
}
