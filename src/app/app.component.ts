import { Component } from '@angular/core';
import { NodeService } from './node.service'

import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'requestImage';
  name='';
  image;
  constructor(private api: NodeService,private http: HttpClient) { }
  imageToShow: any;
  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
       this.imageToShow = reader.result;
    }, false);
 
    if (image) {
       reader.readAsDataURL(image);
    }
 }
  selectImage(event){
    if(event.target.files.length > 0){
      this.image = event.target.files[0];
    }
  }
  getImage(){
    this.http.get("http://localhost:3000/users/givemeimg/"+this.name, { responseType: 'blob' }).subscribe(
        (res)=> this.createImageFromBlob(res),
        (err)=>console.error(err.error)
   
    )}
  setImage(){
    this.api.setImage(this.image);
   
  }
}
